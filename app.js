const fs = require('fs');
const path = require('path');
const express = require("express");
const port = 8080
const morgan = require('morgan')
const folder = './files/'


const app = express();

app.use(express.json())

app.listen(port, (error) => {
    error ? console.log(error) : console.log(`Server is working on ${port} port`)
})

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))


app.get('/', (req, res) => {
    res.send('Hello')
})

app.post('/api/files', (req, res) => {
    const { filename, content } = req.body
    const allFiles = []

    fs.readdir(folder, (error, files) => {
        try {
            files.forEach(file => {
                allFiles.push(file)
            })
            if (allFiles.includes(filename)) {

                res.status(400)
                throw new Error('File with such name already exists');

            } else if (!filename) {

                res.statusMessage = "Please specify 'filename' parameter"
                res.status(400)
                throw new Error("Please specify 'filename' parameter");

            } else if (!content) {

                res.statusMessage = "Please specify 'content' parameter"
                res.status(400)
                throw new Error("Please specify 'content' parameter");

            } else if (content, filename, !allFiles.includes(filename)) {
                res
                    .status(200)
                    .send(JSON.stringify({
                        "message": "File created successfully"
                    }))

                fs.writeFile(`./files/${filename}`, content ? content : '', (err) => {
                    console.log(err);
                })
            } else {

                res.status(500)
                res.statusMessage = 'Server error'
                throw new Error("Server error");

            }

        } catch (error) {
            console.log(error);
            res.send(JSON.stringify({ message: `${error}` }))
        }
    })
})

app.get('/api/files', (req, res) => {
    fs.readdir(folder, (error, files) => {

        if (!error) {
            res.status(200)
            res.send(JSON.stringify({
                message: 'Success',
                files
            }))
        } else {
            res.status(400)
            res.send(error)
        }
    })
})

app.get('/api/files/:filename', (req, res) => {
    const filepath = req.url;
    const fileName = path.basename(filepath);
    const ext = fileName.split('.')[1]

    fs.readdir(folder, (error, files) => {
        let allFiles = []

        files.forEach(file => allFiles.push(file))
        try {
            if (allFiles.includes(fileName)) {
                const content = fs.readFileSync(`${folder}${fileName}`, 'utf-8')
                const modifiedTime = fs.statSync(`${folder}${fileName}`).mtime;
                res.status(200)
                res.send(JSON.stringify({
                    message: "Success",
                    filename: fileName,
                    content: content,
                    extension: ext,
                    uploadedDate: modifiedTime
                }))
            } else {
                res.status(400)
                throw new Error(`No file with '${fileName}' filename found`);
            }
        } catch (error) {
            res.send(JSON.stringify({
                message: `${error}`
            }))
        }

    })
})

console.log(`Server is working`);